#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "complex.h"

#define PLUS_SIGN_ASCII 43
#define MINUS_SIGN_ASCII 45


void read_comp(Complex* complex_number, float real_number, float imaginary_number) {
    complex_number->real_number = real_number;
    complex_number->imaginary_number = imaginary_number;
}

void print_comp(Complex* complex_number) {
    char sign = complex_number->imaginary_number >= 0 ? PLUS_SIGN_ASCII : MINUS_SIGN_ASCII;
    float u_img = fabs(complex_number->imaginary_number);
    printf("%.2f %c (%.2f)i\n", complex_number->real_number, sign, u_img);
}

Complex * add_comp(Complex* first_complex_number, Complex* second_complex_number) {
    Complex * add_operation_complex = malloc(sizeof(Complex));
    add_operation_complex->real_number = first_complex_number->real_number + second_complex_number->real_number;
    add_operation_complex->imaginary_number = first_complex_number->imaginary_number + second_complex_number->imaginary_number;
    return add_operation_complex;
}

Complex * sub_comp(Complex* first_complex_number, Complex* second_complex_number) {
    Complex * sub_operation_complex = malloc(sizeof(Complex));
    sub_operation_complex->real_number = first_complex_number->real_number - second_complex_number->real_number;
    sub_operation_complex->imaginary_number = first_complex_number->imaginary_number - second_complex_number->imaginary_number;
    return sub_operation_complex;
}

Complex * mult_comp_real(Complex* complex_number, float real_number) {
    Complex * mul_operation_complex = malloc(sizeof(Complex));
    mul_operation_complex->real_number = complex_number->real_number * real_number;
    mul_operation_complex->imaginary_number = complex_number->imaginary_number * real_number;
    return mul_operation_complex;
}

Complex * mult_comp_img(Complex* complex_number, float imaginary_number) {
    Complex * mul_operation_complex = malloc(sizeof(Complex));
    mul_operation_complex->real_number =  - (complex_number->imaginary_number * imaginary_number);
    mul_operation_complex->imaginary_number = complex_number->real_number * imaginary_number;
    return mul_operation_complex;
}

Complex * mult_comp_comp(Complex* first_complex_number, Complex* second_complex_number) {
    Complex * mul_operation_complex = malloc(sizeof(Complex));
    mul_operation_complex->real_number =  (first_complex_number->real_number * second_complex_number->real_number) -
    (first_complex_number->imaginary_number * second_complex_number->imaginary_number);
    mul_operation_complex->imaginary_number = (first_complex_number->real_number * second_complex_number->imaginary_number) +
    (first_complex_number->imaginary_number * second_complex_number->real_number);
    return mul_operation_complex;
}

float abs_comp(Complex* complex_number) {
    return sqrt(pow(complex_number->real_number, 2) + pow(complex_number->imaginary_number, 2));
}

