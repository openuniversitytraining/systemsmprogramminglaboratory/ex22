
typedef struct Complex
{
    float real_number;
    float imaginary_number;
} Complex;

void read_comp(Complex* complex_number, float real_number, float imaginary_number);

void print_comp(Complex* complex_number);

Complex * add_comp(Complex* first_complex_number, Complex* second_complex_number);

Complex * sub_comp(Complex* first_complex_number, Complex* second_complex_number);

Complex * mult_comp_real(Complex* complex_number, float real_number);

Complex * mult_comp_img(Complex* complex_number, float imaginary_number);

Complex * mult_comp_comp(Complex* first_complex_number, Complex* second_complex_number);

float abs_comp(Complex* complex_number);

