#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "complex.h"

#define true 1
#define false 0

#define LINE_MAX_INPUT 80
#define FIRST_INDEX 0
#define SECOND_INDEX 1
#define NOT_IN_ARRAY (-1)

#define SPACE ' '
#define COMMA ','
#define TAB '\t'
#define END_OF_STRING '\0'
#define NEW_LINE '\n'
#define EMPTY ""

#define IS_WHITE_CHAR(c) (c == SPACE || c == TAB || c == NEW_LINE)

/*
 -------------------------------------- Input Error Commands --------------------------------------
 These functions print an error message to the console and set valid_command to false if there is an issue with
 the user input. Each function handles a different type of error.
 */
int over_text_in_args() {
    printf("Extraneous text after end of command\n");
    return false;
}

int missing_text_in_args() {
    printf("Missing parameter\n");
    return false;
}

int multiple_consecutive_commas() {
    printf("Multiple consecutive commas\n");
    return false;
}

int missing_comma() {
    printf("Missing comma\n");
    return false;
}

int undefined_command_name() {
    printf("Undefined command name\n");
    return false;
}

int illegal_comma() {
    printf("Illegal comma\n");
    return false;
}

int undefined_complex_variable() {
    printf("Undefined complex variable\n");
    return false;
}

int invalid_parameter() {
    printf("Invalid parameter, not a number\n");
    return false;
}

/*
 * This is a struct that represents user input. It contains the operator code (e.g. "add_comp"), the number of
 * registers and inputs, arrays of Complex objects and floats that represents the registers and input values,
 * and a string of arguments that represent the argument the use inserted.
 */
typedef struct Input{
    char * op_code;
    int registers_amount;
    int inputs_amount;
    Complex* registers[2];
    float inputs[2];
    char * args;
    int valid_input;
} Input;

/*
 * This is a struct that represents a complex number. It contains a Complex object and a name (e.g. "A").
 */
struct ComplexObject{
    Complex complex;
    char* name;
};

/*
 * These are enumerations used to index into arrays of function input objects and register strings.
 */
enum FUNCTION_INPUT_INDEXES {READ_COMP = 0, PRINT_COMP, ADD_COMP, SUB_COMP,  MULT_COMP_REAL, MULT_COMP_IMG, MULT_COMP_COMP, ABS_COMP, STOP, MAX_FUNCTION_INDEX};
enum REGISTERS_ENUM {A = 0, B, C, D, E, F, MAX_REGISTER_INDEX};

/*
 * This is an array of Input objects that contains the valid operator codes
 * and the number of registers and inputs required for each operation.
 */
static struct Input FUNCTION_INPUT_OBJECTS[] = {
        {"read_comp", 1, 2},
        {"print_comp", 1, 0},
        {"add_comp", 2, 0},
        {"sub_comp", 2, 0},
        {"mult_comp_real", 1, 1},
        {"mult_comp_img", 1, 1},
        {"mult_comp_comp", 2, 0},
        {"abs_comp", 1, 0},
        {"stop", 0, 0},
};

/*
 * This is an array of ComplexObject objects that contains the names and initial values of the registers.
 */
static struct ComplexObject REGISTERS_STRINGS[] = {
        {{0, 0}, "A"},
        {{0, 0}, "B"},
        {{0, 0}, "C"},
        {{0, 0}, "D"},
        {{0, 0}, "E"},
        {{0, 0}, "F"}
};

/*
 * This function takes an operator code and returns the index of the corresponding Input
 * object in FUNCTION_INPUT_OBJECTS. If the operator code is not found, it returns NOT_IN_ARRAY.
 */
int get_function_index(char* op_code) {
    int index = 0;
    for (; index < MAX_FUNCTION_INDEX; index++) {
        if (strcmp(FUNCTION_INPUT_OBJECTS[index].op_code, op_code) == 0) {
            break;
        }
    } if (index == MAX_FUNCTION_INDEX) {return NOT_IN_ARRAY;}
    return index;
}

/*
 * This function takes a register name (e.g. "A") and returns the index of the corresponding ComplexObject
 * object in REGISTERS_STRINGS. If the register name is not found, it returns NOT_IN_ARRAY.
 */
int get_complex_object_for_name(char * register_name){
    int index = 0;
    for (; index < MAX_REGISTER_INDEX; index++) {
        if (strcmp(REGISTERS_STRINGS[index].name, register_name) == 0) {
            break;
        }
    } if (index == MAX_REGISTER_INDEX) {return NOT_IN_ARRAY;}
    return index;
}

/*
 * This function takes user input and returns the operator code.
 * It sets valid_command to false if there is an issue with the user input.
 */
int check_valid_and_get_op_code(char * input, char ** op_code) {
    int index = 0;
    int first_char_index = 0;
    int saw_char = false;
    for (; index < strlen(input); ++index) {
        if (input[index] == COMMA) {return illegal_comma();}
        if (!IS_WHITE_CHAR(input[index]) && saw_char == false) {
            first_char_index = index;
            saw_char = true;
        }
        else if (IS_WHITE_CHAR(input[index]) && saw_char == true) {break;}
    }
    *op_code = (char *) malloc(index - first_char_index + 1);
    strncpy(*op_code, input + first_char_index, index - first_char_index);
    (*op_code)[index] = END_OF_STRING;
    if (get_function_index(*op_code) == NOT_IN_ARRAY) {{
        return undefined_command_name();}}
    return true;
}

/*
 * This function takes user input and returns the string of the inserted arguments.
 */
char * get_args(char * input) {
    int i = 0;
    int j;
    char * args;
    int saw__first_char = false;
    for (; i < strlen(input); ++i) {
        if (!IS_WHITE_CHAR(input[i]) && saw__first_char == false) {saw__first_char = true;}
        else if (IS_WHITE_CHAR(input[i]) && saw__first_char == true) { i++; break;}
    }
    if (strlen(input) != i) {
        for (j = strlen(input) - 1; j > i + 1; --j) {
            if (input[j] != SPACE && input[j] != TAB && input[j] != NEW_LINE) {j++;  break; }
        }
        args = (char *) malloc(j - i + 1);
        strncpy(args, input + i, j - i);
        args[j - i] = END_OF_STRING;
        return args;
    }
    return EMPTY;
}

/*
 * This is a helper function. It extracts an argument from input_data->args based on the start
 * and end indexes of the argument and removes the argument from input_data->args.
 */
void set_arg_from_args_indexes(Input * input_data, int index, int white_space_in_start,
                               int white_space_in_end, char ** arg) {
    int len_index = index - white_space_in_start - white_space_in_end;
    if (index > 0) {
        *arg = (char *) malloc(index - white_space_in_start);
        strncpy(*arg, input_data->args + white_space_in_start, len_index);
        (*arg)[len_index] = END_OF_STRING;
        if (strcmp(input_data->args + index, EMPTY) == 0) {
            input_data->args = EMPTY;
        } else {
            input_data->args = input_data->args + index + 1;
        }
    } else {
        *arg =  (char *) malloc(strlen(EMPTY) + 1);
        *arg = EMPTY;
        (*arg)[strlen(EMPTY)] = END_OF_STRING;
    }
}

/*
 * This function takes user input and extracts an argument from input_data->args until a comma is encountered.
 * It also handles errors if there are too many commas or the argument is invalid.
 * It sets valid_command to false if there is an issue with the user input.
 */
int get_arg_until_comma_remove_from_args_and_check_valid(Input * input_data, int buffer, int exp_commas, char ** arg) {
    int index = 0;
    int white_space_in_start = 0;
    int white_space_in_end = 0;
    int saw_white_space = false;
    int saw_first_char = false;
    char prev_char = COMMA;
    for (; index < strlen(input_data->args); ++index) {
        /* Checks if the current character and previous character is a not comma */
        if (input_data->args[index] == COMMA) {
            if (buffer > exp_commas) {return over_text_in_args();}
            else if (prev_char == COMMA){ return multiple_consecutive_commas();}
            else { break;}
        }
        /* Check if we reach the end of the line */
        if (input_data->args[index] == NEW_LINE) {break;}
        /* Checks if the current character is either a space or a tab character.
         * If it is, it increments a counter for whitespace characters seen either at the start or end of the argument.*/
        if (input_data->args[index] == SPACE || input_data->args[index] == TAB) {
            if (saw_first_char) {saw_white_space = true; white_space_in_end++;}
            else {white_space_in_start++;}
        } else { /* Checks if previously seen whitespace characters and a non-whitespace character */
            if (saw_first_char && saw_white_space) {return missing_comma();}
            saw_first_char = true;
            prev_char = input_data->args[index];
        }
    }
    set_arg_from_args_indexes(input_data, index, white_space_in_start, white_space_in_end, &*arg);
    return true;
}

/*
 * This function checks whether a register exists or not.
 * It takes a register name as input and returns a boolean value indicating whether
 * the register exists or not.
 */
int register_exist(char * register_name) {
    int register_index = get_complex_object_for_name(register_name);
    if (register_index == NOT_IN_ARRAY) {
        return undefined_complex_variable();
    } return true;
}

/*
 * This function takes a pointer to a character array as an input,
 * which represents an argument to be validated and checks if the value is a float.
 */
int input_valid(char * arg) {
    int len;
    float dummy = 0;
    if (sscanf(arg, "%f %n", &dummy, &len) != 1 || len != (int)strlen(arg)) {
        return invalid_parameter();
    } return true;
}

/*
 * The parse_registers_check_if_valid function is used to parse a register name from an argument string and
 * store the corresponding register complex value pointer in an Input struct.
 */
int parse_registers_check_if_valid(Input * input_data, int * registers_buffer, char * arg) {
    if (register_exist(arg)) {
        input_data->registers[*registers_buffer] = &REGISTERS_STRINGS[get_complex_object_for_name(arg)].complex;
        *registers_buffer = *registers_buffer + 1;
        return true;
    }
    return false;
}

/*
 * This function parses the input arguments for the program and adds them to the input_data struct.
 */
int parse_inputs_check_if_valid(Input * input_data, int * inputs_buffer, char * arg) {
    if (input_valid(arg)) {
        input_data->inputs[*inputs_buffer] = atof(arg);
        *inputs_buffer = *inputs_buffer + 1;
        return true;
    }
    return false;
}

/*
 * This function is used to parse the input arguments of the user's command.
 * It takes as input a pointer to a struct "Input" that contains the input data of the user's command.
 * The function extracts each argument from the input data one by one until it has parsed all of them.
 */
int parse_args(Input * input_data) {
    int registers_buffer = 0;
    int inputs_buffer = 0;
    char * arg;
    /* Begins a while loop that will continue as long as the args
     * field of the input_data object is not equal to the EMPTY string. */
    while (strcmp(input_data->args, EMPTY) != 0) {
        /* Extracts an argument from the "args" field of the "input_data" object */
        if (!get_arg_until_comma_remove_from_args_and_check_valid(input_data, registers_buffer + inputs_buffer,
                                                                 registers_buffer < input_data->registers_amount +
                                                                                    input_data->inputs_amount,
                                                                 &arg)) { return false; }
        /* If we got empty argument, meaning we are over the allowed text */
        if (strcmp(arg, EMPTY) == 0) {return over_text_in_args();}
        /* Read registers arguments from arguments line */
        if (registers_buffer < input_data->registers_amount) {
            if (!parse_registers_check_if_valid(input_data, &registers_buffer, arg)) {return false;}
        }
        /* Read inputs arguments from arguments line */
        else if (inputs_buffer < input_data->inputs_amount) {
            if (!parse_inputs_check_if_valid(input_data, &inputs_buffer, arg)) {return false;}
        }
        /* If we got another argument, we are over the allowed text */
        else {
            return over_text_in_args();}
    }
    /* If we got short with argument, we print missing_text error */
    if (inputs_buffer + registers_buffer < input_data->registers_amount + input_data->inputs_amount) {
        return missing_text_in_args();
    }
    return true;
}

/*
 * This function receives input from the user.
 */
void get_input_from_user(char * input_line) {
    printf("Please enter command for the program:\n");
    fgets(input_line, LINE_MAX_INPUT, stdin);
    printf("%s", input_line);
}

/*
 * This function reads input from the user, extracts the opcode and arguments, and parses
 * the arguments into Input object. The function returns a pointer to the Input object.
 */
Input * parse_input(Input * input_data){
    char input[LINE_MAX_INPUT];
    int valid_command;
    get_input_from_user(input);
    valid_command = check_valid_and_get_op_code(input, &input_data->op_code);
    if (valid_command == true) {
        *input_data = FUNCTION_INPUT_OBJECTS[get_function_index(input_data->op_code)];
        input_data->args = get_args(input);
        valid_command = parse_args(input_data);
    }
    input_data->valid_input = valid_command;
    return input_data;
}

/*
 * This function takes a pointer to an Input object as an argument and runs the
 * appropriate function based on the op_code value in the Input object.
 */
void run_function_with_input_format_pointer_and_check_is_running(Input * function_object) {
    Complex * temp_complex;
    switch (get_function_index(function_object->op_code)) {
        case READ_COMP:
            read_comp(function_object->registers[FIRST_INDEX],
                      function_object->inputs[FIRST_INDEX],
                      function_object->inputs[SECOND_INDEX]);
            print_comp(function_object->registers[FIRST_INDEX]);
            break;
        case PRINT_COMP:
            print_comp(function_object->registers[FIRST_INDEX]);
            break;
        case ADD_COMP:
            temp_complex = add_comp(function_object->registers[FIRST_INDEX],
                     function_object->registers[SECOND_INDEX]);
            print_comp(temp_complex);
            free(temp_complex);
            break;
        case SUB_COMP:
            temp_complex = sub_comp(function_object->registers[FIRST_INDEX],
                     function_object->registers[SECOND_INDEX]);
            print_comp(temp_complex);
            free(temp_complex);
            break;
        case MULT_COMP_REAL:
            temp_complex = mult_comp_real(function_object->registers[FIRST_INDEX],
                           function_object->inputs[FIRST_INDEX]);
            print_comp(temp_complex);
            free(temp_complex);
            break;
        case MULT_COMP_IMG:
            temp_complex = mult_comp_img(function_object->registers[FIRST_INDEX],
                          function_object->inputs[FIRST_INDEX]);
            print_comp(temp_complex);
            free(temp_complex);
            break;
        case MULT_COMP_COMP:
            temp_complex = mult_comp_comp(function_object->registers[FIRST_INDEX],
                           function_object->registers[SECOND_INDEX]);
            print_comp(temp_complex);
            free(temp_complex);
            break;
        case ABS_COMP:
            printf("%.2f\n", abs_comp(function_object->registers[FIRST_INDEX]));
            break;
        case STOP:
            exit(0);
        default:
            printf("OP code %s is not known!\n", function_object->op_code);
            break;
    }
}

int main() {
    Input * input_data;
    printf("Welcome to my complex program!\n");
    while (true) {
        input_data = malloc(sizeof(Input));
        input_data = parse_input(input_data);
        if (input_data->valid_input == true) {
            run_function_with_input_format_pointer_and_check_is_running(input_data);
        }
        free(input_data);
    }
}
