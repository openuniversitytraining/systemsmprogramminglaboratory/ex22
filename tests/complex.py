import cmath


class Complex:

    def __init__(self, real_number: float = None, imagery_number: float = None,
                 complex_obj: complex = None, is_complex: bool = False):
        if not is_complex:
            self.complex = complex(round(real_number, 2), round(imagery_number, 2))
        else:
            self.complex = complex_obj

    def set_values(self, real_number: float, imagery_number: float) -> None:
        self.complex = complex(real_number, imagery_number)

    def get_print_complex(self) -> str:
        sign = "+" if self.complex.real >= 0 else "-"
        return f"{self.complex.real:.2f} {sign} ({self.complex.imag:.2f})i"

    def get_abs_value(self) -> str:
        return f"{abs(self.complex):.2f}"


def get_add_complexes(first_complex: complex, second_complex: complex) -> Complex:
    return Complex(complex_obj=(first_complex + second_complex), is_complex=True)


def get_sub_complexes(first_complex: complex, second_complex: complex) -> Complex:
    return Complex(complex_obj=(first_complex - second_complex), is_complex=True)


def get_mul_complex_with_real(complex_number: complex, mul_number: float) -> Complex:
    return Complex(complex_obj=(complex_number * mul_number), is_complex=True)


def get_mul_complex_with_imag(complex_number: complex, mul_number: float) -> Complex:
    return Complex(complex_obj=(complex_number * complex(0, mul_number)), is_complex=True)


def get_mul_complex_with_complex(first_complex: complex, second_complex: complex) -> Complex:
    return Complex(complex_obj=(first_complex * second_complex), is_complex=True)
