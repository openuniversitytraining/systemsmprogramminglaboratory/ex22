import math

from complex import Complex


class ProgramCommands:
    class Commands:
        PRINT = "print_comp"
        READ = "read_comp"
        ADD = "add_comp"
        SUB = "sub_comp"
        MUL_REAL = "mult_comp_real"
        MUL_IMG = "mult_comp_img"
        MUL_COMP = "mult_comp_comp"
        ABS = "abs_comp"
        STOP = "stop"

    def read_comp(self, register_name: str, real_number: float, imag_number: float) -> str:
        return f"{self.Commands.READ} {register_name}, {real_number}, {imag_number}"

    def print_comp(self, register_name: str) -> str:
        return f"{self.Commands.PRINT} {register_name}"

    def add_comp(self, first_register_name: str, second_register_name: str) -> str:
        return f"{self.Commands.ADD} {first_register_name}, {second_register_name}"

    def sub_comp(self, first_register_name: str, second_register_name: str) -> str:
        return f"{self.Commands.SUB} {first_register_name}, {second_register_name}"

    def mult_comp_real(self, first_register_name: str, real_number: float) -> str:
        return f"{self.Commands.MUL_REAL} {first_register_name}, {real_number}"

    def mult_comp_img(self, first_register_name: str, img_number: float) -> str:
        return f"{self.Commands.MUL_IMG} {first_register_name}, {img_number}"

    def mult_comp_comp(self, first_register_name: str, second_register_name: str) -> str:
        return f"{self.Commands.MUL_COMP} {first_register_name}, {second_register_name}"

    def abs_comp(self, first_register_name: str) -> str:
        return f"{self.Commands.ABS} {first_register_name}"

    def stop(self) -> str:
        return f"{self.Commands.STOP}"

    @staticmethod
    def _get_exp_number(number: float) -> list:
        optional_numbers = []
        number_as_float = round(number, 2)
        optional_numbers.append(round(number_as_float + 0.01, 2))
        optional_numbers.append(round(number_as_float - 0.01, 2))
        optional_numbers.append(round(number_as_float, 2))
        return optional_numbers

    def validate_print_comp(self, complex_number: Complex, command_output: str):
        imag_sign = "+" if complex_number.complex.imag >= 0 else "-"
        real_sign = "+" if complex_number.complex.real >= 0 else "-"
        if complex_number.complex.real < 0:
            command_output = command_output[1:]
        command_output = command_output.split(imag_sign)
        output_real_number = command_output[0]
        output_imag_number = command_output[1]
        chars_to_remove = ["(", ")", "i", " "]
        for char in chars_to_remove:
            output_imag_number = output_imag_number.replace(char, "")
        exp_real_number = self._get_exp_number(complex_number.complex.real)
        exp_imag_number = self._get_exp_number(complex_number.complex.imag)
        assert float(real_sign + output_real_number) in exp_real_number
        assert float(imag_sign + output_imag_number) in exp_imag_number

    @staticmethod
    def validate_abs(complex_number: Complex, command_output: str):
        exp_real_number = round(float(command_output), 2)
        assert float(complex_number.get_abs_value()) == exp_real_number
