import os
import subprocess
from pathlib import Path
from typing import Union

TEST_TEMP_INPUT_FILE_PATH = "input_file"
TEST_TEMP_OUTPUT_FILE_PATH = "output_file"
TEMP_OUTPUT_DIR_NAME = "input_and_outputs_files"


def run_program_with_file_anf_get_result(executable_file: str, input_file: Union[str, Path],
                                         output_file: Union[str, Path]) -> str:
    process = subprocess.Popen(f"{executable_file} < {input_file} > {output_file}", shell=True)
    process.wait()
    data = ""
    with open(output_file, "r") as file:
        for line in file.readlines():
            data += line
    return data


def create_test_output_dir(dir_name: str, subdir_name: str) -> Path:
    output_path = Path(TEMP_OUTPUT_DIR_NAME)
    if not output_path.exists():
        os.mkdir(output_path)
    output_dir_path = output_path.joinpath(dir_name)
    test_output_dir_path = output_dir_path.joinpath(subdir_name)
    if not output_dir_path.exists():
        os.mkdir(output_dir_path)
    os.mkdir(test_output_dir_path)
    return test_output_dir_path


def get_line_after_command(res: str, command: str) -> str:
    lines = res.split("\n")
    for index, line in enumerate(lines):
        if command in line:
            return lines[index + 1]
    raise Exception("Didnt fount command in results!")

