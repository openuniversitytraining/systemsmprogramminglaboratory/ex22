import pytest
import random
from typing import Tuple

from complex import *
from generic_functions import *
from mycomp_commands import ProgramCommands

EXECUTABLE_FILE_PATH = "./tests/mycomp"

TEST_INPUT_FILE = "input"
TEST_OUTPUT_FILE = "output"

MIN_VALUE = -100
MAX_VALUE = 100
EXECUTION_NUMBER = 50

REGISTERS = {
    "A": Complex(real_number=0, imagery_number=0),
    "B": Complex(real_number=0, imagery_number=0),
    "C": Complex(real_number=0, imagery_number=0),
    "D": Complex(real_number=0, imagery_number=0),
    "E": Complex(real_number=0, imagery_number=0),
    "F": Complex(real_number=0, imagery_number=0),
}
REGISTER_LIST = list(REGISTERS.keys())

program_commands = ProgramCommands()


def write_command_to_file(input_file_path: Path, command: str) -> None:
    with open(input_file_path.as_posix(), "w") as input_file_path:
        input_file_path.write(f"{command}\n")
        input_file_path.write(f"{ProgramCommands().stop()}\n")


def write_list_of_commands_to_file(input_file_path: Path, commands: list) -> None:
    with open(input_file_path, "w") as input_file:
        for command in commands:
            input_file.write(f"{command}\n")
        input_file.write(f"{ProgramCommands().stop()}\n")


def run_commands_get_res(test_name: str, subdir_name: str, commands: list):
    test_output_dir_path = create_test_output_dir(test_name, subdir_name)
    input_file_path = test_output_dir_path.joinpath(TEST_INPUT_FILE)
    output_file_path = test_output_dir_path.joinpath(TEST_OUTPUT_FILE)
    write_list_of_commands_to_file(input_file_path, commands)
    return run_program_with_file_anf_get_result(EXECUTABLE_FILE_PATH, input_file_path, output_file_path)


def get_random_register() -> str:
    register_name = random.choice(REGISTER_LIST)
    real_number = round(random.uniform(MIN_VALUE, MAX_VALUE), 2)
    imag_number = round(random.uniform(MIN_VALUE, MAX_VALUE), 2)
    REGISTERS[register_name] = Complex(real_number=real_number, imagery_number=imag_number)
    return register_name


def get_registers_names_and_result_for_two_complex(command_name: str, iteration: int, command_function: callable) \
        -> Tuple[str, str, str]:
    first_register_name = get_random_register()
    second_register_name = get_random_register()
    command_list = [
        program_commands.read_comp(first_register_name, REGISTERS[first_register_name].complex.real,
                                   REGISTERS[first_register_name].complex.imag),
        program_commands.read_comp(second_register_name, REGISTERS[second_register_name].complex.real,
                                   REGISTERS[second_register_name].complex.imag),
        command_function(first_register_name, second_register_name)
    ]
    result = run_commands_get_res(command_name, str(iteration), command_list)
    line_after = get_line_after_command(result, command_name)
    return first_register_name, second_register_name, line_after


def get_register_name_and_result_for_complex_and_number(command_name: str, iteration: int, command_function: callable) \
        -> Tuple[str, float, str]:
    register_name = get_random_register()
    number = round(random.uniform(MIN_VALUE, MAX_VALUE), 2)
    command_list = [
        program_commands.read_comp(register_name, REGISTERS[register_name].complex.real,
                                   REGISTERS[register_name].complex.imag),
        command_function(register_name, number)
    ]
    result = run_commands_get_res(command_name, str(iteration), command_list)
    line_after = get_line_after_command(result, command_name)
    return register_name, number, line_after


class TestMyCompDefaultCommands:
    @pytest.mark.parametrize("execution_number", range(EXECUTION_NUMBER))
    def test_read_comp(self, execution_number: int):
        register_name = get_random_register()
        command_list = [program_commands.read_comp(
            register_name, REGISTERS[register_name].complex.real, REGISTERS[register_name].complex.imag)]
        result = run_commands_get_res(program_commands.Commands.READ, str(execution_number), command_list)
        line_after = get_line_after_command(result, program_commands.Commands.READ)
        program_commands.validate_print_comp(REGISTERS[register_name], line_after)

    @pytest.mark.parametrize("execution_number", range(EXECUTION_NUMBER))
    def test_add_comp(self, execution_number: int):
        first_register_name, second_register_name, line_after = get_registers_names_and_result_for_two_complex(
            program_commands.Commands.ADD, execution_number, program_commands.add_comp)
        program_commands.validate_print_comp(
            get_add_complexes(
                REGISTERS[first_register_name].complex, REGISTERS[second_register_name].complex), line_after)

    @pytest.mark.parametrize("execution_number", range(EXECUTION_NUMBER))
    def test_sub_comp(self, execution_number: int):
        first_register_name, second_register_name, line_after = get_registers_names_and_result_for_two_complex(
            program_commands.Commands.SUB, execution_number, program_commands.sub_comp)
        program_commands.validate_print_comp(
            get_sub_complexes(
                REGISTERS[first_register_name].complex, REGISTERS[second_register_name].complex), line_after)

    @pytest.mark.parametrize("execution_number", range(EXECUTION_NUMBER))
    def test_mul_real(self, execution_number: int):
        first_register_name, number, line_after = get_register_name_and_result_for_complex_and_number(
            program_commands.Commands.MUL_REAL, execution_number, program_commands.mult_comp_real)
        program_commands.validate_print_comp(
            get_mul_complex_with_real(REGISTERS[first_register_name].complex, number), line_after)

    @pytest.mark.parametrize("execution_number", range(EXECUTION_NUMBER))
    def test_mul_imag(self, execution_number: int):
        first_register_name, number, line_after = get_register_name_and_result_for_complex_and_number(
            program_commands.Commands.MUL_IMG, execution_number, program_commands.mult_comp_img)
        program_commands.validate_print_comp(
            get_mul_complex_with_imag(REGISTERS[first_register_name].complex, number), line_after)

    @pytest.mark.parametrize("execution_number", range(EXECUTION_NUMBER))
    def test_mul_comp(self, execution_number: int):
        first_register_name, second_register_name, line_after = get_registers_names_and_result_for_two_complex(
            program_commands.Commands.MUL_COMP, execution_number, program_commands.mult_comp_comp)
        program_commands.validate_print_comp(
            get_mul_complex_with_complex(
                REGISTERS[first_register_name].complex, REGISTERS[second_register_name].complex), line_after)

    @pytest.mark.parametrize("execution_number", range(EXECUTION_NUMBER))
    def test_abs(self, execution_number: int):
        register_name = get_random_register()
        command_list = [
            program_commands.read_comp(register_name, REGISTERS[register_name].complex.real,
                                       REGISTERS[register_name].complex.imag),
            program_commands.abs_comp(register_name)
        ]
        result = run_commands_get_res(program_commands.Commands.ABS, str(execution_number), command_list)
        line_after = get_line_after_command(result, program_commands.Commands.ABS)
        program_commands.validate_abs(REGISTERS[register_name], line_after)
